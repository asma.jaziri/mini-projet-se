#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>


void listFiles(const char *path);

int main()
{
    // Le chemin du répertoire à lister
    char path[100];
    
    // Input path from user
    printf("Entrer le chemin du répertoire à lister: ");
    scanf("%s", path);
    
    listFiles(path);
    
    return 0;
}


/**
 * Lister tous les fichiers et sous dossiers du répertoire.
 */
void listFiles(const char *path)
{
    struct dirent *dp;
    DIR *dir = opendir(path);
    
    // Test d'accès au répertoire
    if (!dir)
        return;
    
    while ((dp = readdir(dir)) != NULL)
    {
        printf("%s\n", dp->d_name);
    }
    
    // Fermer le répertoire
    closedir(dir);
}
