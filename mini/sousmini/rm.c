#include <stdio.h>

int main()
{
    int status;
    char file_name[255];
    
    printf("Entrer le chemin et le nom du fichier ou du dossier à supprimer\n");
    scanf("%254s",file_name);
 //gets(file_name);
    
    status = remove(file_name);
    
    if (status == 0)
        printf("\n%s \nFichier/dossier supprimé avec succès.\n", file_name);
    else
    {
        printf("Impossible de supprimer le fichier/dossier. \n");
        perror("Following error occurred");
    }
    
    return 0;
}

