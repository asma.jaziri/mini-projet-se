#include <stdio.h>
#include <stdlib.h>
#include<sys/stat.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/param.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

int main()
{

    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Répertoire courant: ");

        printf("%s", cwd);
        printf("$ \n");
    } else {
        perror("getcwd() error");
    }
   }
