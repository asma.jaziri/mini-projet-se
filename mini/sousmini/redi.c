#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>


void listFiles(const char *path, char file[100]);

int main()
{
    char path[100];
    char file[100];
    
    printf("Entrez le répertoire à lister: ");
    scanf("%s", path);
   
    printf("Entrez le nom fichier: ");// creation du fichier dans lequel nous allons stocker le output
    scanf("%s", file);
    //strcat(path,file);
    printf("%s",file);
    listFiles(path, file);
    
    return 0;
}

/**
 * Lister tous les fichiers et sous dossiers du répertoire.
 */
void listFiles(const char *path, char file[100])
{FILE *fptr;
    char filename[100], c;
    
    FILE * finout1;
    finout1= fopen(file,"w");
    
    struct dirent *dp;
    DIR *dir = opendir(path);
    
    // Test d'accès au répertoire
    if (!dir)
        return;
    
    while ((dp = readdir(dir)) != NULL)
    {
            fprintf(finout1,"%s\n", dp->d_name);// stockage dans le fichier créé
    }
    fclose(finout1);

    
    // Fermer le répertoire
    closedir(dir);
}






